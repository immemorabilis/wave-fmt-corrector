// Add the following to your Cargo.toml under [dependencies]:
// binrw = "0.8.0"
// color-eyre = "0.5.11"
use binrw::BinResult;
use std::path::Path;

use binrw::{
    io::{Read, Seek},
    BinRead, BinWrite,
};
use color_eyre::Result;

#[derive(BinWrite, Debug)]
/// The `ParsedFile` represents a .wav file with a `Wave` block in front and the `fmt ` block immediately following.
/// Since the program is only supposed to correct the format block, it leaves the rest of the bytes untouched (in `rest`).
struct ParsedFile {
    riff: Wave,
    fmt: Fmt,
    rest: Vec<u8>,
}

impl BinRead for ParsedFile {
    type Args = ();

    fn read_options<R: Read + Seek>(
        reader: &mut R,
        options: &binrw::ReadOptions,
        args: Self::Args,
    ) -> BinResult<Self> {
        // Read a the `Wave` and `fmt ` blocks normally...
        let riff = Wave::read(reader)?;
        let fmt = Fmt::read(reader)?;

        // ... but use a Vec<u8> that is filled until EOF for the rest.
        // Afaik, binrw doesn't support reading a Vec<T> without providing a count of elements,
        // so this is a workaround.
        let mut rest = Vec::new();

        // While you can read a byte, do so...
        while let Ok(byte) = u8::read(reader) {
            rest.push(byte);
        }

        Ok(Self { riff, fmt, rest })
    }
}

impl ParsedFile {
    /// This function reads and parses a .wav file by reading the file at `path`.
    fn read_from_file<P>(path: P) -> BinResult<Self>
    where
        P: AsRef<Path>,
    {
        let mut file = std::fs::File::open(path)?;
        ParsedFile::read(&mut file)
    }

    /// This function writes a .wav file to the file at `path`.
    fn write_to_file<P>(&self, path: P) -> Result<()>
    where
        P: AsRef<Path>,
    {
        let mut out_file = std::fs::File::create(path)?;
        self.write_to(&mut out_file)?;

        Ok(())
    }
}

#[derive(BinRead, BinWrite, Debug)]
#[brw(magic(b"RIFF"))]
struct Wave {
    chunk_size: u32,
}

#[derive(BinRead, BinWrite, Debug, Copy, Clone)]
#[brw(magic(b"WAVEfmt "))]
struct Fmt {
    chunk_size: u32,
    format: Format,
    mode: Mode,
    sample_rate: u32,
    bytes_per_second: u32,
    bytes_per_sample: u16,
    bits_per_sample: u16,
}

impl Fmt {
    // This is unused, but shows how converting the `Mode` to a `u32` would look like
    fn num_channels(&self) -> u32 {
        match self.mode {
            Mode::Mono => 1,
            Mode::Stereo => 2,
            _ => 0,
        }
    }

    fn calculate_bytes_per_second(&mut self) {
        // Convert the `Mode` into a `u32`, by using the values representing the `enum`s variants.
        // Mono = 1,
        // Stereo = 2
        let num_channels = self.mode as u32;

        self.bytes_per_second = self.sample_rate * num_channels * (self.bits_per_sample as u32) / 8;
    }

    fn calculate_bytes_per_sample(&mut self) {
        // Convert the `Mode` into a `u32`, by using the values representing the `enum`s variants.
        // Mono = 1,
        // Stereo = 2
        let num_channels = self.mode as u32;

        self.bytes_per_sample = (num_channels * (self.bits_per_sample as u32) / 8) as u16;
    }
}

#[derive(BinRead, BinWrite, Debug, Copy, Clone)]
#[brw(repr = u16)]
enum Format {
    Pcm = 1,
    Invalid = 0,
}

#[derive(BinRead, BinWrite, Debug, Copy, Clone)]
#[brw(repr = u16)]
enum Mode {
    Mono = 1,
    Stereo = 2,
    Invalid = 0,
}

fn main() -> Result<()> {
    let incorrect = ParsedFile::read_from_file("in.wav")?;

    let corrected_fmt = {
        // Change this according to your .wav file.
        //
        // the corrected `fmt` looks like the `incorrect.fmt` in every way,
        // except for the `mode`, the `sample_rate`, and the `bits_per_sample`.
        let mut fmt = Fmt {
            mode: Mode::Stereo,
            sample_rate: 44100,
            bits_per_sample: 16,
            ..incorrect.fmt
        };
        fmt.calculate_bytes_per_second();
        fmt.calculate_bytes_per_sample();

        fmt
    };

    let corrected = ParsedFile {
        fmt: corrected_fmt,
        ..incorrect
    };

    corrected.write_to_file("out.wav")?;
    println!("{:?}", corrected.fmt);

    Ok(())
}
